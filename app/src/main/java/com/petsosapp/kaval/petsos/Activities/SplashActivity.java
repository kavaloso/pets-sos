package com.petsosapp.kaval.petsos.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.petsosapp.kaval.petsos.R;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                goNext();
            }
        }, SPLASH_TIME_OUT);
    }

    private void goNext() {
        Intent intent = new Intent(this,SignInActivity.class);

//        if(!UserPreferencesManager.getInstance(this).getPreferenceSession().isEmpty()){
//            intent = new Intent(this,HomeActivity.class);
//        }else{
//            if (UserPreferencesManager.getInstance(this).getAuthCode().isEmpty()) {
//                intent = new Intent(this, SignInAuthCode.class);
//            }else{
//                intent = new Intent(this, SignInActivity.class);
//            }
//        }
        startActivity(intent);
        finish();
    }
}
