package com.petsosapp.kaval.petsos.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.petsosapp.kaval.petsos.Models.Posts;
import com.petsosapp.kaval.petsos.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by USUARIO on 05/11/2016.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.NewsHolder>  {
    private List<Posts> data;
    private Context context;

    public PostAdapter(List<Posts> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.news_row, parent, false);
        return new NewsHolder(v);
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {
        Posts posts = data.get(position);
        holder.tv_user_name.setText(posts.getUsername());
        holder.tv_news_description.setText(posts.getDescription());
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(holder.iv_news);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class NewsHolder extends RecyclerView.ViewHolder{

        TextView tv_user_name;
        TextView tv_news_description;
        ImageView iv_news;

        public NewsHolder(View itemView) {
            super(itemView);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_news_description = (TextView) itemView.findViewById(R.id.tv_news_description);
            iv_news = (ImageView) itemView.findViewById(R.id.iv_news);
        }
    }
}
