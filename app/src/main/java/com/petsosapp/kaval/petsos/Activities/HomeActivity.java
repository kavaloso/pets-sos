package com.petsosapp.kaval.petsos.Activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.petsosapp.kaval.petsos.Adapters.PagerAdapter;
import com.petsosapp.kaval.petsos.R;

public class HomeActivity extends AppCompatActivity {

    TabLayout tabs;
    ViewPager pager;
    PagerAdapter adapter;
    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_actitivy);

        setupTabs();
    }

    private void setupTabs() {
        tabs = (TabLayout) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new PagerAdapter(getSupportFragmentManager());

        pager.setAdapter(adapter);
        tabs.setupWithViewPager(pager);

        //nombre a pestañas
        tabs.getTabAt(0).setIcon(ContextCompat.getDrawable(this,R.drawable.public_message));
        tabs.getTabAt(1).setIcon(ContextCompat.getDrawable(this,R.drawable.rectangular_speech_balloon));
        tabs.getTabAt(2).setIcon(ContextCompat.getDrawable(this,R.drawable.male_user_profile_picture));

        fragmentManager = getSupportFragmentManager();

        tabs.setTabTextColors(ContextCompat.getColorStateList(this, R.color.tab_selector));
        tabs.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.white ));

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
    }
}
