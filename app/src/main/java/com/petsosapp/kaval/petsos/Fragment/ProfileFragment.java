package com.petsosapp.kaval.petsos.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petsosapp.kaval.petsos.Adapters.PostAdapter;
import com.petsosapp.kaval.petsos.Adapters.ProfileAdapter;
import com.petsosapp.kaval.petsos.Models.Posts;
import com.petsosapp.kaval.petsos.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 07/11/2016.
 */

public class ProfileFragment extends Fragment {

    private RecyclerView rv_profile;
    List<Posts> user_post;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user_post = new ArrayList<>();
        //for header
        user_post.add(new Posts("Kevin Avalos","1 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","2 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","3 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","4 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","5 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","6 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","7 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","8 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        user_post.add(new Posts("Kevin Avalos","9 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile,container,false);
        setupViews(v);
        return v;
    }

    public void setupViews(View v) {
        rv_profile = (RecyclerView) v.findViewById(R.id.rv_profile);
        ProfileAdapter adapter = new ProfileAdapter(user_post,getActivity());
        rv_profile.setAdapter(adapter);
        rv_profile.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
    }
}
