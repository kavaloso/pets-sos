package com.petsosapp.kaval.petsos.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.petsosapp.kaval.petsos.Models.Posts;
import com.petsosapp.kaval.petsos.R;

import java.util.List;

/**
 * Created by tk-0130 on 11/8/16.
 */

public class ProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Posts> data;
    private Context context;

    public ProfileAdapter(List<Posts> data, Context context) {
        this.data = data;
        this.context = context;
        //for header
        data.add(0,null);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        RecyclerView.ViewHolder viewHolder;
        if(viewType == 0){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_rv_profile,parent,false);
            viewHolder = new HeaderHolder(v);
        }else{
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row,parent,false);
            viewHolder = new ItemHolder(v);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ItemHolder){
            final int p = holder.getAdapterPosition();
            Posts post = data.get(p);
            ((ItemHolder) holder).tv_user_name.setText(post.getUsername());
            ((ItemHolder) holder).tv_news_description.setText(post.getDescription());
        }else if(holder instanceof  HeaderHolder){
            ((HeaderHolder) holder).bt_header_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context,"CLICK!",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder{

        TextView tv_user_name;
        TextView tv_news_description;
        ImageView iv_news;

        public ItemHolder(View itemView) {
            super(itemView);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_news_description = (TextView) itemView.findViewById(R.id.tv_news_description);
            iv_news = (ImageView) itemView.findViewById(R.id.iv_news);
        }
    }

    public class HeaderHolder extends RecyclerView.ViewHolder{

        Button bt_header_profile;
        LinearLayout ll_write_comment;

        public HeaderHolder(View itemView) {
            super(itemView);
            bt_header_profile = (Button) itemView.findViewById(R.id.bt_header_profile);
            ll_write_comment = (LinearLayout) itemView.findViewById(R.id.ll_write_comment);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return 0;
        }
        return 1;
    }
}
