package com.petsosapp.kaval.petsos.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petsosapp.kaval.petsos.Adapters.PostAdapter;
import com.petsosapp.kaval.petsos.Models.Posts;
import com.petsosapp.kaval.petsos.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 05/11/2016.
 */

public class NewsFragment extends Fragment {

    private RecyclerView rv_news;
    List<Posts> posts_ls;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        posts_ls = new ArrayList<>();
        posts_ls.add(new Posts("Kevin Avalos","1 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor. "));
        posts_ls.add(new Posts("Kevin Avalos","2 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        posts_ls.add(new Posts("Kevin Avalos","3 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        posts_ls.add(new Posts("Kevin Avalos","4 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        posts_ls.add(new Posts("Kevin Avalos","5 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        posts_ls.add(new Posts("Kevin Avalos","6 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        posts_ls.add(new Posts("Kevin Avalos","7 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
        posts_ls.add(new Posts("Kevin Avalos","8 Hola, soy Tobi... me perdi en San Martin de Porres el día martes 1 de noviembre, soy chusquito color blanco, si me encuentras conmunicate con mi dueño por favor."));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_news,container,false);
        setupViews(v);
        return v;
    }

    private void setupViews(View v) {
        rv_news = (RecyclerView) v.findViewById(R.id.rv_news);
        PostAdapter adapter = new PostAdapter(posts_ls,getActivity());
        rv_news.setAdapter(adapter);
        rv_news.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
    }
}
