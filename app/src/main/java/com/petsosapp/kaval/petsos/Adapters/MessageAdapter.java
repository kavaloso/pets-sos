package com.petsosapp.kaval.petsos.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.petsosapp.kaval.petsos.Models.Message;
import com.petsosapp.kaval.petsos.R;

import java.util.List;

/**
 * Created by USUARIO on 06/11/2016.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.NewsHolder>  {

    List<Message> data;
    Context context;

    public MessageAdapter(List<Message> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.messages_row, parent, false);
        return new NewsHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {
        Message message = data.get(position);
        holder.tv_username_message.setText(message.getUsername());
        if(message.getRead()){
            holder.iv_read.setVisibility(View.INVISIBLE);
        }else{
            holder.iv_read.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class NewsHolder extends RecyclerView.ViewHolder{

        TextView tv_username_message;
        ImageView iv_read;

        public NewsHolder(View itemView) {
            super(itemView);
            tv_username_message = (TextView) itemView.findViewById(R.id.tv_username_message);
            iv_read = (ImageView) itemView.findViewById(R.id.iv_read);
        }
    }
}
