package com.petsosapp.kaval.petsos.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.petsosapp.kaval.petsos.Fragment.MessagesFragment;
import com.petsosapp.kaval.petsos.Fragment.NewsFragment;
import com.petsosapp.kaval.petsos.Fragment.ProfileFragment;


/**
 * Created by USUARIO on 26/04/2016.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    static final int cantidad_tabs = 3;
    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new NewsFragment();
            case 1:
                return new MessagesFragment();
            case 2:
                return new ProfileFragment();
            default:
                return new NewsFragment();
        }
    }

    @Override
    public int getCount() {
        return cantidad_tabs;
    }
}
