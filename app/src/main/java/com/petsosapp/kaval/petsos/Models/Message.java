package com.petsosapp.kaval.petsos.Models;

/**
 * Created by USUARIO on 06/11/2016.
 */

public class Message {
    String username;
    String profileImage;
    String message;
    Boolean read;

    public Message(String username, String message, Boolean read) {
        this.username = username;
        this.message = message;
        this.read = read;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }
}
