package com.petsosapp.kaval.petsos.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petsosapp.kaval.petsos.Adapters.MessageAdapter;
import com.petsosapp.kaval.petsos.Models.Message;
import com.petsosapp.kaval.petsos.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 06/11/2016.
 */

public class MessagesFragment extends Fragment {

    private RecyclerView rv_messages;
    private List<Message> messages_ls;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        messages_ls = new ArrayList<>();
        messages_ls.add(new Message("KEVIN AVALOS","HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA",false));
        messages_ls.add(new Message("JENNY VEGA","HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA",false));
        messages_ls.add(new Message("JEROMY LLERENA","HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA",true));
        messages_ls.add(new Message("JHOSELYN PALACIOS","HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA",false));
        messages_ls.add(new Message("JHON BARRANTES","HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA HOLA",true));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_messages,container,false);
        setupViews(v);
        return v;
    }

    private void setupViews(View v){
        rv_messages = (RecyclerView) v.findViewById(R.id.rv_messages);
        System.out.println("FRAGMENT MESSAGE\n");
        for (Message m : messages_ls) {
            System.out.println(m.getUsername());
        }
        MessageAdapter adapter = new MessageAdapter(messages_ls,getActivity());
        rv_messages.setAdapter(adapter);
        rv_messages.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
    }
}
